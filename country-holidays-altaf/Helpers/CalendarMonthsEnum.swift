//
//  CalendarMonthsEnum.swift
//  country-holidays-altaf
//
//  Created by Mohammed Ashraf on 03/02/2022.
//

import Foundation

enum CalendarMonthsEnum: String, CaseIterable {
    case January, February, March, April, May, June, July, August, September, October, November, December
}
