//
//  Constant.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 02/02/22
//

import Foundation


struct Constant {
    static let holidayList                =           "Holiday List"
    static let name                       =           "Name"
    static let nation                     =           "Nation"
    static let date                       =           "Date"
    static let weekDay                    =           "Week Day"
    static let India                      =           "India"
    static let IndiaCode                  =           "IN"
    static let done                       =           "Done"
    static let pleaseWait                 =           "Please Wait..."
    static let search                     =           "Search"
    static let searchCountry              =           "Search Country"
    static let serviceUrlFailed           =           "ServiceURL conversion to URL failed unexpectedly"
    static let someThingWentWrong         =           "Some thing Went Wrong"
    static let freeTrialYear              =           "2020"
    static let noHolidaysSelectedMonth    =           "No Holidays for the selected Month"
    
}

struct StoryboardName {
    static let main                            =           "Main"
}

struct viewControllerName {
    static let holidaysViewController          =           "HolidaysViewController"
    static let countrySearchViewController     =           "CountrySearchViewController"

}

struct xibName {
    static let holidayListTableCell            =           "HolidayListTableCell"
    static let cell                            =           "cell"

}

struct assetsName {
    static let dropDownImg                     =            "dropDownImg"
    static let stytemImageMagnifyingglass      =            "magnifyingglass"
}

struct dateFormats {
    static let MMMM                            =           "MMMM"
    static let MM                              =           "MM"
}

struct jsonFileName {
    static let mockCountriesList               =           "MockCountriesList"
    static let MockFebHolidayListIN            =           "MockFebHolidayListIN"
}

struct errorTitle {
    static let unableToDecodeHolidaysListJson  = "Unable to decode holidays list json"
    static let pageTitleShouldNotBeNil         = "Page title should not be nil"
    static let holidayList                     = "Holiday List"
    static let pageTitleShouldRetunHolidayList = "Page title should retun as Holiday List"
    static let onceCountryNameTappedCountrySearchViewControllerPresented = "Once country name is tapped, CountrySearchViewController should be presented"
    static let holidaysListTableViewShouldNotNil = "Holidays list tableView should not be nil"
    static let holidaysListTableViewDelegateShouldNotNil = "Holidays list tableView Delegate should not be nil"
    static let holidaysListTableViewShouldConfirmDataSource = "Holidays list tableView should confirm to DataSource"
    static let holidaysListVCShouldContainIdentiderNameHolidayListTableCell = "HolidaysListVC Should contain the identifier with name HolidayListTableCell"
    static let numberRowsShouldReturnExpectedFromStubModelObject = "Number of rows should return as expected from stub model object."
    static let HolidayListTableCellAt0RowShouldContainSameHolidayNameStubDataModelObject = "HolidayListTableCell at 0th row should contain the same holiday name as per the Stub Data Model Object."
    static let numberOfRowsShouldReturn0HolidayListNotAvailable = "Number of rows should return 0 as the holiday list is not available."
    static let acutalTotalMonthsValueShouldMatchWithExpectedValueOf12 = "Acutal total months value should match with expected value of 12"
    static let acutalMonthNameAtGivenPositionShouldMatchExpectedName = "Acutal month name at given position should match with expected name"
    static let unableToDecodeCountryListjson = "Unable to decode Country list json"
    static let countriesListTableViewShouldNotBeNil = "Countries list tableView should not be nil"
    static let countryListTableViewDelegateShouldNotBeNil = "Country List TableView Delegate Should not be nil"
    static let CountryListTableViewShouldConfirmDataSource = "Country List TableView Should confirm to DataSource"
}
