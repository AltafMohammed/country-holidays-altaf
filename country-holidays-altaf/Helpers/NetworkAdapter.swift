//
//  NetworkAdapter.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 03/02/22
//

import Foundation
import Alamofire

struct ServiceURL {
    enum Path {
        case holidayUrl(countryCode: String,month: String, year: String)
        case countrysUrl
        fileprivate var components: String {
            switch self {
            case let .holidayUrl(countryCode,month,year):
                return "https://holidays.abstractapi.com/v1/?api_key=d9f70dbfbecc4087af992bce60a1f740&country=\(countryCode)&year=\(year)&month=\(month)"
            case .countrysUrl:
                return "https://api.printful.com/countries"
            }
        }
    }
    private let path: Path
    
    init(path: Path) {
        self.path = path
    }
    
    var url: URL {
        guard let url = URL(string: path.components) else { fatalError(Constant.serviceUrlFailed) }
        return url
    }
}
struct NetworkAdapter {
    //MARK: - API method
    static func fetchDataFromServer(url: URL, method: String, parameters: [String: Any]?, headers: HTTPHeaders, closure:@escaping (_ response: Any?, _ status : Bool, _ error: String?) -> Void) {
        var httpMethod: HTTPMethod = .get
        switch method {
        case HttpMethodTypes.get:
            httpMethod = .get
            break
        case HttpMethodTypes.post:
            httpMethod = .post
            break
        default:
            break
        }
        AF.request(url, method: httpMethod, parameters: parameters ?? nil, encoding: JSONEncoding.default, headers: headers)
            .responseJSON { response in
                print( "\n==================================================\nURL : \(url) \n==================================================")
                print(response)
                switch response.result {
                case .success(_):
                    if response.response?.statusCode == 200 || response.response?.statusCode == 201 {
                        closure(response,true,nil)
                    } else if response.response?.statusCode == 422 ||  response.response?.statusCode == 404 ||  response.response?.statusCode == 500 {
                        closure(response.response?.statusCode, false, response.error?.localizedDescription ?? Constant.someThingWentWrong)
                    }
                case .failure(let error):
                    closure(nil, false, error.localizedDescription)
                    debugPrint(" error: \(error)")
                }
            }
    }
}

//MARK: - HTTPMethod method
struct HttpMethodTypes {
    static let get     = "GET"
    static let post    = "POST"
}
