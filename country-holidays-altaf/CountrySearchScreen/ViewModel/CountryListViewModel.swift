//
//  CountryListViewModel.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 03/02/22
//

import Foundation
import Alamofire

class CountryListViewModel {
    
    var countryList: [CountryData] = []
    var delegate: CountryListAPIProtocol?
    
    func getAllCountryListAPICall() {
        let url =  ServiceURL.init(path: .countrysUrl).url
        let headers: HTTPHeaders = [
            "content-type" : "application/json"
        ]
        NetworkAdapter.fetchDataFromServer(url: url, method: HttpMethodTypes.get, parameters: nil, headers: headers) { (response, isSuccess, error) in
            if isSuccess {
                if let _response = response as? AFDataResponse<Any> {
                    if let data = _response.data {
                        do {
                            let decode = JSONDecoder()
                            let model = try decode.decode(CountryListModel.self, from: data)
                            self.countryList = model.countryData ?? []
                            self.delegate?.getAllCountryList(isSuccess:true, error: nil)
                        } catch let errorString {
                            print(errorString.localizedDescription)
                            self.delegate?.getAllCountryList(isSuccess:true, error: errorString.localizedDescription)
                        }
                    }
                }
            } else {
                print(error ?? "")
                self.delegate?.getAllCountryList(isSuccess:true, error: nil)
            }
        }
        
    }
    // MARK: Get the Country list count
    func getCountryListCount() -> Int {
        return countryList.count
    }
}
