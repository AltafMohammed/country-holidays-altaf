//
//  CountryListModel.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 03/02/22
//

import Foundation

struct CountryListModel: Codable {
    let code: Int?
    let countryData: [CountryData]?
    
    enum CodingKeys: String, CodingKey {
        case code
        case countryData = "result"
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        code = try values.decodeIfPresent(Int.self, forKey: .code)
        countryData = try values.decodeIfPresent([CountryData].self, forKey: .countryData)
    }
}

struct CountryData: Codable {
    let name: String?
    let code: String?
    
    enum CodingKeys: String, CodingKey {
        case name
        case code
    }
    
    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        code = try values.decodeIfPresent(String.self, forKey: .code)
    }
}
