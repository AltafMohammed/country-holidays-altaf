//
//  CountrySearchViewController.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 03/02/22
//

import UIKit

protocol SelectedCounrtyDelegate {
    func selectedCountryData(name: String, code: String)
}
class CountrySearchViewController: UIViewController {

    // MARK: Outlets
    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var countryTableView: UITableView!
    
    // MARK: ViewModel declaration and data assigning
    let viewModel = CountryListViewModel()
    var isFromSearch = false
    var searchCountryList: [CountryData] = []
    var delegate: SelectedCounrtyDelegate?
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setStyles()
    }
    
    //MARK: Set ViewStyles
    func setStyles() {
        let systemSearchImage = UIImage(systemName: assetsName.stytemImageMagnifyingglass)
        let imageV = UIImageView(image: systemSearchImage)
        imageV.contentMode = .scaleToFill
        imageV.frame = CGRect(x: 0, y: 0, width: 18, height: 18)
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 10, width: 25, height: 18))
        paddingView.addSubview(imageV)
        searchTextField.rightView = paddingView
        searchTextField.rightViewMode = .always
        searchTextField.placeholder = Constant.searchCountry
        self.viewModel.delegate = self
        DispatchQueue.main.async {
            self.countryTableView.delegate = self
            self.countryTableView.dataSource = self
            self.countryTableView.register(UITableViewCell.self, forCellReuseIdentifier: xibName.cell)
            self.getCountryListCall()
        }
    }
    
    func refreshCountriesListTableView() {
        self.countryTableView.reloadData()
    }
    
    private func getCountryListCall() {
        self.showLoading(message: Constant.pleaseWait)
        viewModel.getAllCountryListAPICall()
    }
}

// MARK: TableView Delegates and Datasource Methods
extension CountrySearchViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return isFromSearch ? self.searchCountryList.count : viewModel.getCountryListCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: xibName.cell) else {
            return UITableViewCell()
        }
        var model = viewModel.countryList[indexPath.row]
        if isFromSearch {
            model = self.searchCountryList[indexPath.row]
        }
        cell.textLabel?.text = model.name
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var model = viewModel.countryList[indexPath.row]
        if isFromSearch {
            model = self.searchCountryList[indexPath.row]
        }
        delegate?.selectedCountryData(name: model.name ?? "", code: model.code ?? "")
        dismiss(animated: true, completion: nil)
    }
}

// MARK: UITextField Delegates
extension CountrySearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        if let textTypeCasteString = textField.text as NSString? {
            let searchStr = textTypeCasteString.replacingCharacters(in: range, with: string)
            if !searchStr.isEmpty {
                self.isFromSearch = true
                self.searchCountryList = viewModel.countryList.filter {
                    let countries = $0.name?.uppercased()
                    return countries?.contains(searchStr.uppercased()) ?? true
                }
                countryTableView.reloadData()
            }
        }
        return true
    }
}

// MARK: API Delegate Method to fetch the Country list
extension CountrySearchViewController: CountryListAPIProtocol {
    func getAllCountryList(isSuccess: Bool, error: String?) {
        self.hideLoading()
        if isSuccess {
            self.refreshCountriesListTableView()
        }
    }
    
}
