//
//  CountryListAPIProtocol.swift
//  country-holidays-altaf
//
//  Created by Mohammed Ashraf on 09/02/2022.
//

import Foundation

protocol CountryListAPIProtocol {
    func getAllCountryList(isSuccess: Bool, error: String?)
}
