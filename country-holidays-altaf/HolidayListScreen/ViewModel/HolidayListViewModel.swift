//
//  HolidayListViewModel.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 02/02/22
//


import Foundation
import Alamofire

class HolidayListViewModel {
    var holidayModel: [HolidayModel] = []
    var selectedMonth: Int? = 2
    var delegate: HolidayListViewModelAPI?

    func getHolidayListBasedOnMonthYear(countryCode: String, month: String, year: String) {
        let url =  ServiceURL.init(path: .holidayUrl(countryCode: countryCode, month: month, year: year)).url
        let headers: HTTPHeaders = [
            "content-type" : "application/json"
        ]
        NetworkAdapter.fetchDataFromServer(url: url, method: HttpMethodTypes.get, parameters: nil, headers: headers) { (response, isSuccess, error) in
            if isSuccess {
                if let _response = response as? AFDataResponse<Any> {
                    if let data = _response.data {
                        do {
                            self.holidayModel.removeAll()
                            let decode = JSONDecoder()
                            let list = try decode.decode([HolidayModel].self, from: data)
                            self.holidayModel.append(contentsOf: list)
                            self.delegate?.getResponseForHolidayList(isSuccess: true, error: nil)
                        } catch let errorString {
                            print(errorString.localizedDescription)
                            self.delegate?.getResponseForHolidayList(isSuccess: false, error: errorString.localizedDescription)
                        }
                    }
                }
            } else {
                print(error ?? "")
                self.delegate?.getResponseForHolidayList(isSuccess: false, error: error ?? "")
            }
        }
    }

    // MARK: Get the holiday list count
    func getHolidaysCount() -> Int {
        return self.holidayModel.count
    }
    
    func getCalendarMonthsCount() -> Int {
        return CalendarMonthsEnum.allCases.count
    }
    
    func getPrefixNameForSeletedMonth(position: Int) -> String {
        let titleName = self.holidayModel[position].name ?? ""
        return titleName
    }
    
    func getCalendarMonthName(at position: Int) -> String {
        switch position {
        case 0:
            return CalendarMonthsEnum.January.rawValue
        case 1:
            return CalendarMonthsEnum.February.rawValue
        case 2:
            return CalendarMonthsEnum.March.rawValue
        case 3:
            return CalendarMonthsEnum.April.rawValue
        case 4:
            return CalendarMonthsEnum.May.rawValue
        case 5:
            return CalendarMonthsEnum.June.rawValue
        case 6:
            return CalendarMonthsEnum.July.rawValue
        case 7:
            return CalendarMonthsEnum.August.rawValue
        case 8:
            return CalendarMonthsEnum.September.rawValue
        case 9:
            return CalendarMonthsEnum.October.rawValue
        case 10:
            return CalendarMonthsEnum.November.rawValue
        case 11:
            return CalendarMonthsEnum.December.rawValue
        default:
            return ""
        }
    }
}
