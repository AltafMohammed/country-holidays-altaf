//
//  HolidayListViewModelAPI.swift
//  country-holidays-altaf
//
//  Created by Mohammed Ashraf on 03/02/2022.
//

import Foundation

protocol HolidayListViewModelAPI {
    func getResponseForHolidayList(isSuccess: Bool, error: String?)
}


