//
//  HolidaysViewController.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 02/02/22
//

import UIKit

class HolidaysViewController: UIViewController {
    
    // MARK: Outlets
    @IBOutlet weak var selectOptionsView: UIView!
    @IBOutlet weak var countryView: UIView!
    @IBOutlet weak var countryRightArrowImg: UIImageView!
    @IBOutlet weak var countryLbl: UILabel!
    @IBOutlet weak var holidayListTableView: UITableView!
    @IBOutlet weak var dateView: UIView!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var searchHolidayBtn: UIButton!

    // MARK: ViewModel declaration and data assigning
    var viewModel = HolidayListViewModel()
    var selectedCountryCode = Constant.IndiaCode
    
    // MARK: Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        self.LoadViewStyle()
        self.viewModel.delegate = self
        self.getHolidayListCall()
    }
    
    //MARK: Set ViewStyles
    func LoadViewStyle() {
        self.title = Constant.holidayList
        self.view.backgroundColor = .systemGroupedBackground
        self.selectOptionsView.layer.cornerRadius = 5.0
        self.countryView.layer.cornerRadius = 5.0
        self.countryView.layer.borderWidth = 0.7
        self.countryView.layer.borderColor = UIColor.lightGray.cgColor
        self.countryRightArrowImg.setImageColor(color: .darkGray)
        dateView.layer.cornerRadius = 5.0
        dateView.layer.borderWidth = 0.7
        dateView.layer.borderColor = UIColor.lightGray.cgColor
        searchHolidayBtn.layer.borderWidth = 0.7
        searchHolidayBtn.layer.cornerRadius = 5.0
        searchHolidayBtn.layer.borderColor = UIColor.lightGray.cgColor
        searchHolidayBtn.setTitle(Constant.search, for: .normal)
        searchHolidayBtn.backgroundColor = .blue.withAlphaComponent(0.5)
        searchHolidayBtn.setTitleColor(.white, for: .normal)
        holidayListTableView.backgroundColor = .systemGroupedBackground
        holidayListTableView.separatorStyle = .none
        holidayListTableView.register(UINib(nibName: xibName.holidayListTableCell, bundle: nil), forCellReuseIdentifier: xibName.holidayListTableCell)
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(selectCountyTaped))
        self.countryView.addGestureRecognizer(tapGesture)
        self.countryLbl.text = Constant.India
        self.setCurrentMonth()
        
    }
    
    //MARK: Set Current month to UIPicker
    private func setCurrentMonth() {
        let imageV = UIImageView(image: UIImage(named: assetsName.dropDownImg))
        imageV.contentMode = .scaleAspectFit
        imageV.frame = CGRect(x: 0, y: 0, width: 15, height: 15)
        let paddingView: UIView = UIView.init(frame: CGRect(x: 0, y: 0, width: 20, height: 15))
        paddingView.addSubview(imageV)
        dateTextField.rightView = paddingView
        dateTextField.rightViewMode = .always
        self.dateTextField.text = Date().getMonth(dateFormat: dateFormats.MMMM)
        viewModel.selectedMonth = Int(Date().getMonth(dateFormat: dateFormats.MM) ?? "") ?? 0

        let pickerView = UIPickerView()
        pickerView.dataSource = self
        pickerView.delegate = self
        dateTextField.inputView = pickerView
        addDoneButtonOnKeyboard()
    }
    
    @objc func selectCountyTaped() {
        if let countryVC = UIStoryboard(name: StoryboardName.main, bundle: nil).instantiateViewController(withIdentifier: viewControllerName.countrySearchViewController) as? CountrySearchViewController {
            countryVC.delegate = self
            self.navigationController?.present(countryVC, animated: true)
        }
    }
    
    // MARK: Set UIPicker ToolBar
    func addDoneButtonOnKeyboard() {
        let doneToolbar: UIToolbar = UIToolbar(frame: CGRect.init(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        doneToolbar.barStyle = .default
        
        let flexSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: nil, action: nil)
        
        let done: UIBarButtonItem = UIBarButtonItem(title: Constant.done, style: .done, target: self, action: #selector(self.dismissKeyboard))
        let items = [flexSpace, done]
        doneToolbar.items = items
        doneToolbar.sizeToFit()
        self.dateTextField.inputAccessoryView = doneToolbar
    }
    
    @objc func dismissKeyboard() {
        self.dateTextField.resignFirstResponder()
    }
    
    //MARK: Holiday List ApiCall
    private func getHolidayListCall() {
        self.showLoading(message: Constant.pleaseWait)
        self.viewModel.getHolidayListBasedOnMonthYear(countryCode: self.selectedCountryCode, month: "\(viewModel.selectedMonth ?? 2)", year: Constant.freeTrialYear)
    }
    
    func refreshHolidaysListTableView() {
        self.holidayListTableView.reloadData()
    }
    
    @IBAction func searchSelectedDateHolidayBtnAction(_ sender: Any) {
        self.getHolidayListCall()
    }
}

// MARK: TableView Delegates and Datasource Methods
extension HolidaysViewController: UITableViewDataSource, SelectedCounrtyDelegate {
    func selectedCountryData(name: String, code: String) {
        self.countryLbl.text = name
        self.selectedCountryCode = code
        self.getHolidayListCall()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if viewModel.getHolidaysCount() == 0 {
            tableView.emptyMessage(message: Constant.noHolidaysSelectedMonth)
            return 0
        }
        tableView.emptyMessage(message: "")
        return viewModel.getHolidaysCount()
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: xibName.holidayListTableCell) as? HolidayListTableCell else { return UITableViewCell() }
        if !viewModel.holidayModel.isEmpty {
            let model = viewModel.holidayModel[indexPath.row]
            cell.setViewData(model: model)
        }
        return cell
    }
}

// MARK: PickerView Delegates and Datasource Methods
extension HolidaysViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return viewModel.getCalendarMonthsCount()
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return viewModel.getCalendarMonthName(at: row)
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        self.dateTextField.text = viewModel.getCalendarMonthName(at: row)
        viewModel.selectedMonth = row + 1
    }
}

// MARK: API Delegate Method to fetch the Holiday list
extension HolidaysViewController: HolidayListViewModelAPI {
    func getResponseForHolidayList(isSuccess: Bool, error: String?) {
        self.hideLoading()
        if isSuccess {
            self.refreshHolidaysListTableView()
        } 
    }

}
