//
//  HolidayListTableCell.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 02/02/22
//

import UIKit

class HolidayListTableCell: UITableViewCell {

    // MARK: Outlets
    @IBOutlet private weak var borderView: UIView!
    @IBOutlet  weak var nameLbl: UILabel!
    @IBOutlet private weak var nameValueLBl: UILabel!
    @IBOutlet private weak var dateLbl: UILabel!
    @IBOutlet private weak var dateValueLbl: UILabel!
    @IBOutlet private weak var weekDaysLbl: UILabel!
    @IBOutlet private weak var weekDaysVauleLbl: UILabel!
    @IBOutlet private weak var nationLbl: UILabel!
    @IBOutlet private weak var nationValueLbl: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.loadViewStyle()
    }
  
    // MARK: Set View styles
    private func loadViewStyle() {
        nameLbl.text = Constant.name
        dateLbl.text = Constant.date
        weekDaysLbl.text = Constant.weekDay
        nationLbl.text = Constant.nation
        borderView.layer.cornerRadius = 5
        contentView.backgroundColor = .systemGroupedBackground
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func setViewData(model: HolidayModel) {
        nameValueLBl.text = model.name
        dateValueLbl.text = model.date
        weekDaysVauleLbl.text = model.week_day
        nationValueLbl.text = model.location
    }
}
