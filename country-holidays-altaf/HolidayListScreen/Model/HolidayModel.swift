//
//  HolidayModel.swift
//  country-holidays-altaf
//
//  Created by Mohammed Altaf on 03/02/22
//

import Foundation

struct HolidayModel: Codable {
    let name: String?
    let name_local: String?
    let language: String?
    let description: String?
    let country: String?
    let location: String?
    let type: String?
    let date: String?
    let date_year: String?
    let date_month: String?
    let date_day: String?
    let week_day: String?

    enum CodingKeys: String, CodingKey {

        case name
        case name_local
        case language
        case description
        case country
        case location
        case type
        case date
        case date_year
        case date_month
        case date_day
        case week_day
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        name = try values.decodeIfPresent(String.self, forKey: .name)
        name_local = try values.decodeIfPresent(String.self, forKey: .name_local)
        language = try values.decodeIfPresent(String.self, forKey: .language)
        description = try values.decodeIfPresent(String.self, forKey: .description)
        country = try values.decodeIfPresent(String.self, forKey: .country)
        location = try values.decodeIfPresent(String.self, forKey: .location)
        type = try values.decodeIfPresent(String.self, forKey: .type)
        date = try values.decodeIfPresent(String.self, forKey: .date)
        date_year = try values.decodeIfPresent(String.self, forKey: .date_year)
        date_month = try values.decodeIfPresent(String.self, forKey: .date_month)
        date_day = try values.decodeIfPresent(String.self, forKey: .date_day)
        week_day = try values.decodeIfPresent(String.self, forKey: .week_day)
    }
}
