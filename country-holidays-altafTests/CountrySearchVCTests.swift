//
//  CountrySearchViewControllerTests.swift
//  country-holidays-altafTests
//
//  Created by Mohammed Altaf on 07/02/22.
//

import XCTest
@testable import country_holidays_altaf

class CountrySearchViewControllerTests: XCTestCase {
    var CountrySearchViewController: CountrySearchViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: StoryboardName.main, bundle: nil)
        self.CountrySearchViewController = (storyBoard.instantiateViewController(withIdentifier: viewControllerName.countrySearchViewController) as! CountrySearchViewController)
        self.CountrySearchViewController.loadView()
        self.CountrySearchViewController.viewDidLoad()
    }
    
    func mockCountriesList() -> [CountryData] {
        do {
            if let bundlePath = Bundle(for: type(of: self)).path(forResource: jsonFileName.mockCountriesList, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
               let decodedData = try JSONDecoder().decode([CountryData].self, from: jsonData)
               return decodedData
            }
        } catch {
            XCTFail(errorTitle.unableToDecodeCountryListjson)
        }
        return []
    }
    
    
    override class func tearDown() {
        super.tearDown()
    }
    
    func testCountrySearchViewControllerHasTableView() {
        XCTAssertNotNil(CountrySearchViewController.countryTableView, errorTitle.countriesListTableViewShouldNotBeNil)
    }
    
    func testCountrySearchViewControllerHasTableViewDelegate() {
        XCTAssertNotNil(CountrySearchViewController.conforms(to: UITableViewDelegate.self), errorTitle.countryListTableViewDelegateShouldNotBeNil)
    }
    
    func testCountrySearchViewControllerShouldConfirmToTableViewDataSource() {
        XCTAssertTrue(CountrySearchViewController.conforms(to: UITableViewDataSource.self), errorTitle.CountryListTableViewShouldConfirmDataSource)
    }
}
