//
//  HolidaysListScreenTests.swift
//  country-holidays-altafTests
//
//  Created by Mohammed Altaf on 07/02/22.
//

import XCTest
@testable import country_holidays_altaf

class HolidaysListScreenTests: XCTestCase {
    
    var holidaysListVC: HolidaysViewController!
    
    override func setUp() {
        super.setUp()
        
        let storyBoard = UIStoryboard(name: StoryboardName.main, bundle: nil)
        self.holidaysListVC = (storyBoard.instantiateViewController(withIdentifier: viewControllerName.holidaysViewController) as! HolidaysViewController)
        self.holidaysListVC.loadView()
        self.holidaysListVC.viewDidLoad()
    }
    
    override class func tearDown() {
        super.tearDown()
    }
    
    func mockFebHolidaysListForIN() -> [HolidayModel] {
        do {
            if let bundlePath = Bundle(for: type(of: self)).path(forResource: jsonFileName.MockFebHolidayListIN, ofType: "json"),
               let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
               let decodedData = try JSONDecoder().decode([HolidayModel].self, from: jsonData)
               return decodedData
            }
        } catch {
            XCTFail(errorTitle.unableToDecodeHolidaysListJson)
        }
        return []
    }
    
    func testHolidaysViewControllerPageTitleShouldReturnHolidaysList() {
        let pageTitle = holidaysListVC.title
        XCTAssertNotNil(pageTitle, errorTitle.pageTitleShouldNotBeNil)
        XCTAssertEqual(pageTitle, errorTitle.holidayList, errorTitle.pageTitleShouldRetunHolidayList)
    }
    
    
    func testSelectContryShouldNavigateToCountrySearchScreen() {
        UIApplication.shared.keyWindow?.rootViewController = UINavigationController(rootViewController: self.holidaysListVC)
        holidaysListVC.selectCountyTaped()
        XCTAssertTrue(holidaysListVC.presentedViewController?.isKind(of: CountrySearchViewController.self) ?? false, errorTitle.onceCountryNameTappedCountrySearchViewControllerPresented)
    }
    
    func testHolidaysViewControllerHasTableView() {
        XCTAssertNotNil(holidaysListVC.holidayListTableView, errorTitle.holidaysListTableViewShouldNotNil)
    }
    
    func testHolidaysViewControllerHasTableViewDelegate() {
        XCTAssertNotNil(holidaysListVC.holidayListTableView.delegate, errorTitle.holidaysListTableViewDelegateShouldNotNil)
    }
    
    func testHolidaysViewControllerShouldConfirmToTableViewDataSource() {
        XCTAssertTrue(holidaysListVC.conforms(to: UITableViewDataSource.self), errorTitle.holidaysListTableViewShouldConfirmDataSource)
    }
    
    func testHolidaysViewControllerShouldHaveCellIdenifierWithNameHolidayListTableCell() {
        let cell = holidaysListVC.tableView(holidaysListVC.holidayListTableView, cellForRowAt: IndexPath(row: 0, section: 0)) as? HolidayListTableCell
        let actualReuseIdentifer = cell?.reuseIdentifier
        let expectedReuseIdentifier = xibName.holidayListTableCell
        XCTAssertEqual(actualReuseIdentifer, expectedReuseIdentifier, errorTitle.holidaysListVCShouldContainIdentiderNameHolidayListTableCell)
    }
    
    func testHolidaysViewControllerNumberOfRows_WithStubData() {
        let stubHolidaysList = mockFebHolidaysListForIN()
        holidaysListVC.viewModel.holidayModel = stubHolidaysList
        holidaysListVC.refreshHolidaysListTableView()
        let expectedRows = 5
        let actualRows = holidaysListVC.holidayListTableView.numberOfRows(inSection: 0)
        XCTAssertEqual(actualRows, expectedRows, errorTitle.numberRowsShouldReturnExpectedFromStubModelObject)
    }
    
    func testHolidaysViewControllerTableView_CellAtRow0_ShouldContainValuesAsPerStubModel() {
        let stubHolidaysList = mockFebHolidaysListForIN()
        holidaysListVC.viewModel.holidayModel = stubHolidaysList
        let actualHolidayName = holidaysListVC.viewModel.getPrefixNameForSeletedMonth(position: 0)
        let expectedHolidayNameFromStub = "Guru Ravidas Jayanti"
        XCTAssertEqual(actualHolidayName, expectedHolidayNameFromStub, errorTitle.HolidayListTableCellAt0RowShouldContainSameHolidayNameStubDataModelObject)
    }
    
    func testHolidaysListVCTableView_WithNoHolidayData_ShouldReturnZeroRows() {
        holidaysListVC.viewModel.holidayModel = []
        holidaysListVC.refreshHolidaysListTableView()
        let expectedRows = 0
        let actualRows = holidaysListVC.holidayListTableView.numberOfRows(inSection: 0)
        XCTAssertEqual(actualRows, expectedRows, errorTitle.numberOfRowsShouldReturn0HolidayListNotAvailable)
    }
    
    func testHolidaysListVC_ShouldContainMonthsCountAs12() {
        let expectedValue = 12
        let actualValue = holidaysListVC.viewModel.getCalendarMonthsCount()
        XCTAssertEqual(expectedValue, actualValue, errorTitle.acutalTotalMonthsValueShouldMatchWithExpectedValueOf12)
    }
    
    func testHolidaysListVC_MonthNameShouldReturnJanForPosition0() {
        let expectedMonthName = CalendarMonthsEnum.January.rawValue
        let actualValue = holidaysListVC.viewModel.getCalendarMonthName(at: 0)
        XCTAssertEqual(expectedMonthName, actualValue, errorTitle.acutalMonthNameAtGivenPositionShouldMatchExpectedName)
    }
    
}
